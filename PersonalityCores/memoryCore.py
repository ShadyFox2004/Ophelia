#!/usr/bin/python
#memoryCore.py

import os
import discord
#import random
import sqlite3

client = discord.Client()

db = "QandA.db"

def doFormatString(myStr=""):
    myStr = myStr.replace("OEA ",'',1)
    Q,A = myStr.split(" learn ")
    print("My question was : " + Q)
    print("My answer is : " + A)
    return(Q,A)

def doSearchMemory(Q):
    print("Searching memory for answer...")
    myDb = sqlite3.connect(db)
    myRequest = ('''SELECT A FROM IQA WHERE Q = "{}";''').format(Q)
    myCursor = myDb.execute(myRequest)
    myAnswer=""
    for rows in myCursor:
        myAnswer = myAnswer.join(rows)
    myDb.close()
    return(myAnswer)

def doAddToMemory(Q,A):
    print("Adding new entry ...")
    myDb = sqlite3.connect(db)
    myRequest = ('''INSERT INTO IQA (Q,A) VALUES ("{}","{}");''').format(Q,A)
    myDb.execute(myRequest)
    myDb.commit()
    print("New entry added with success.")
    myDb.close()
    return()

def doLookAnswer(request):
    if " learn " in str(request) :
        Q,A = doFormatString(request)
        out = doSearchMemory(Q)
        if out != '':
            print("The answer is : " + str(Out))
            print("I already learned that you fool...")
            return("I already learned that you fool...")
        else:
            print("I am registering the answer...")
            doAddToMemory(Q,A)
            return("I am registering the answer...")
    else:
        Q = request.replace("OEA ",'',1)
        out = doSearchMemory(Q)
        return(out)

@client.event
async def on_ready():
    print(f'{client.user} has connected to Discord!')

@client.event
async def on_message(message):
    out = doLookAnswer(message.content)
    if out != '':
        await message.channel.send(str(out))

client.run(
